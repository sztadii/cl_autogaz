var brands;

brands = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
    makeSlider();
  });

  //private functions
  var init = function () {
    $el = $(".brands");
    $slider = $el.find(".brands__slider");
  };

  var makeSlider = function () {
    $el.imagesLoaded({background: true}).always(function () {
      $slider.slick({
        infinite: true,
        dots: false,
        arrows: true,
        appendArrows: $('.brands__arrows'),
        prevArrow: '<div class="brands__arrow"><i class="fa fa-angle-left"></i></div>',
        nextArrow: '<div class="brands__arrow"><i class="fa fa-angle-right"></i></div>',
        fade: false,
        speed: 1000,
        autoplay: true,
        autoplaySpeed: 1000,
        draggable: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        slidesToShow: 5,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      });
    });
  }
};