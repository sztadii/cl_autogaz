var accordion;

accordion = new function () {

  //catch DOM
  var $el;
  var $item;
  var $itemTop;

  //bind events
  $(document).ready(function () {
    init();
    makeAccordion();
  });

  //private functions
  var init = function () {
    $el = $(".accordion");
    $item = $el.find('.accordion__item');
    $itemTop = $item.find('.accordion__top');
  };

  var makeAccordion = function () {
    $itemTop.on('click', function () {
      var $parent = $(this).parent();
      $parent.toggleClass('-active');
      $parent.siblings().removeClass('-active');
    });
  };
};