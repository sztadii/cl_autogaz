var gallery;

gallery = new function () {

  //catch DOM
  var $el;
  var $box;

  //bind events
  $(document).ready(function () {
    init();
    makeGallery();
  });

  //private functions
  var init = function () {
    $el = $(".gallery");
    $box = $el.find(".gallery__box");
  };

  var makeGallery = function () {
    $box.lightGallery({
      mode: 'lg-slide'
    });
  };
};