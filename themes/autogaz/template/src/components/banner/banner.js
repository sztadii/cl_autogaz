var banner;

banner = new function () {

  //catch DOM
  var $el;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).scroll(function () {
    animation.fading($el.find('.banner__header'));
  });

  //private functions
  var init = function () {
    $el = $('.banner');
  };
};
