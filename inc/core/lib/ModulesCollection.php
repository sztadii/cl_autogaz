<?php
    /**
    * This file is part of Batflat ~ the lightweight, fast and easy CMS
    * 
    * @author       Paweł Klockiewicz <klockiewicz@sruu.pl>
    * @author       Wojciech Król <krol@sruu.pl>
    * @copyright    2017 Paweł Klockiewicz, Wojciech Król <Sruu.pl>
    * @license      https://batflat.org/license
    * @link         https://batflat.org
    */ 

	namespace Inc\Core\Lib;

	/**
	 * Batflat modules collection
	 */
	class ModulesCollection
	{
		/**
		 * List of loaded modules
		 *
		 * @var array
		 */
		protected $modules = [];

		/**
		 * ModulesCollection constructor
		 *
		 * @param \Inc\Core\Main $core
		 */
		public function __construct($core)
		{
			$modules = array_column($core->db('modules')->asc('sequence')->toArray(), 'dir');

			foreach($modules as $dir)
			{
				$file = MODULES.'/'.$dir.'/Admin.php';
				if(file_exists($file))
				{
	                $clsName = 'Admin';
	        		$namespace = 'inc\modules\\'.$dir.'\\'.$clsName;
	                $this->modules[$dir] = new $namespace($core);
					$this->modules[$dir]->init();
	            }
			}
		}

		/**
		 * Get list of modules as array
		 *
		 * @return array
		 */
		public function getArray()
		{
			return $this->modules;
		}

		/**
		 * Check if collection has loaded module
		 *
		 * @param string $name
		 * @return bool
		 */
		public function has($name)
		{
			return array_key_exists($name, $this->modules);
		}

		/**
		 * Get specified module by magic method
		 *
		 * @param string $module
		 * @return \Inc\Core\BaseModule
		 */
		public function __get($module)
		{
			if(isset($this->modules[$module]))
				return $this->modules[$module];
			else
				return null;
		}
	}
