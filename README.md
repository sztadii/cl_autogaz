# To setup template:
* Change dir to template `cd themes/studio/template`
* Install Node.js dependencies with `npm run build`
* Start dev `npm run dev`
* Build prod `npm run prod`

#To start your website:
* Start PHP server `php -S localhost:8000`